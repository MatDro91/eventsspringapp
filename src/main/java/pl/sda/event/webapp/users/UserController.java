package pl.sda.event.webapp.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserController {

    private UserRegistrationService userRegistrationService;
    private UserRegistrationRepository userRegistrationRepository;

    @Autowired
    public UserController(UserRegistrationService userRegistrationService, UserRegistrationRepository userRegistrationRepository) {
        this.userRegistrationService = userRegistrationService;
        this.userRegistrationRepository = userRegistrationRepository;
    }

    @GetMapping(value = "/register")
    public String registration(Model model) {
        model.addAttribute("registrationForm", new RegistrationForm());

        return "register";
    }

    @PostMapping(value = "/register")
    public String registration(@ModelAttribute @Valid RegistrationForm registrationForm, BindingResult bindingResult, Model model) {
        System.out.println(registrationForm);
        System.out.println(registrationForm.getEmail());
        if (bindingResult.hasErrors()) {
            return "register";
        }
        try {
            userRegistrationService.registerUser(registrationForm);
        } catch (UserExistsException e) {
            bindingResult.rejectValue("email", "user.exists", "Użytkownik już istnieje");
            return "register";
        }
        model.addAttribute("userEmail", registrationForm.getEmail());
        return "registerResult";
    }

    @GetMapping(value = "/login")
    public String login() {
        return "login";
    }



}
