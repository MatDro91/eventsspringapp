package pl.sda.event.webapp.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRegistrationDAO {
    private UserRegistrationRepository userRegistrationRepository;

    @Autowired
    public UserRegistrationDAO(UserRegistrationRepository userRegistrationRepository) {
        this.userRegistrationRepository = userRegistrationRepository;
    }

    public boolean userExists(String email) {
        if (userRegistrationRepository.findUserByEmail(email) != null) {
            return true;
        }
        return false;
    }

    public void saveNewUser(User user) {
        if (userExists(user.getEmail())) {
            throw new UserExistsException("Użytkownik o takim meilu " + user.getEmail() + "istnieje.");
        }
        userRegistrationRepository.save(user);
    }
}
