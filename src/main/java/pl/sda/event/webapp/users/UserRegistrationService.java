package pl.sda.event.webapp.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.expression.Sets;

import java.util.HashSet;

@Service
public class UserRegistrationService {

    private UserRegistrationRepository userRegistrationRepository;
    private UserRegistrationDAO userRegistrationDAO;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private RoleRepository roleRepository;

    @Autowired
    public UserRegistrationService(UserRegistrationRepository userRegistrationRepository, BCryptPasswordEncoder bCryptPasswordEncoder, UserRegistrationDAO userRegistrationDAO, RoleRepository roleRepository) {
        this.userRegistrationRepository = userRegistrationRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRegistrationDAO = userRegistrationDAO;
        this.roleRepository = roleRepository;
    }


//    @Autowired
//    public UserRegistrationService(UserRegistrationService userRegistrationService) {
//        this.userRegistrationService = userRegistrationService;
//    }

    public User rewriteRegisterFormToUser(RegistrationForm registrationForm) {
        User user = new User();
        user.setEmail(registrationForm.getEmail());
        user.setNickName(registrationForm.getNickName());
        user.setPassword(bCryptPasswordEncoder.encode(registrationForm.getPassword()));
        return user;
    }

    public void registerUser(RegistrationForm registrationForm) {
        User user = rewriteRegisterFormToUser(registrationForm);
        Role userRole = roleRepository.findRoleByRoleName("ROLE_USER");

        if (userRole == null) {
            userRole = new Role("ROLE_USER");
            roleRepository.save(userRole);
        }
        user.setRoles(new HashSet<>());
        user.getRoles().add(userRole);
        userRegistrationDAO.saveNewUser(user);
    }
}


