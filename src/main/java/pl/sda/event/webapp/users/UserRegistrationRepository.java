package pl.sda.event.webapp.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRegistrationRepository extends JpaRepository<User,Integer> {

    User findUserByEmail(String email);
}
