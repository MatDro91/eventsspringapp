package pl.sda.event.webapp.eventRegistration;

import lombok.Getter;
import lombok.Setter;
import pl.sda.event.webapp.events.Event;
import pl.sda.event.webapp.users.User;

import javax.persistence.*;
import java.util.Date;

@Entity

public class EventRegistration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Date dateOfRegistration;
    @ManyToOne
    private User user;
    @ManyToOne
    private Event event;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(Date dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
