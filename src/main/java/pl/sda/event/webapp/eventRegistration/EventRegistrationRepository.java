package pl.sda.event.webapp.eventRegistration;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRegistrationRepository extends JpaRepository<EventRegistration, Integer> {
    EventRegistration findFirstByUserEmail(String userEmail);

    List<EventRegistration>findAll();

    EventRegistration findFirstByUserEmailAndEventId(String userEmail, Integer eventId);
}
