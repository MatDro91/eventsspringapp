package pl.sda.event.webapp.events;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
    @Query(value = "SELECT e FROM Event e WHERE e.title LIKE %?1%  ORDER BY e.fromDate")
    List<Event> findEventByEventAndTitleLike(String query);

    @Query(value = "SELECT e FROM Event e WHERE e.toDate >= current_date")
    List<Event> findEventByToDate(String period);

    @Query(value = "SELECT e FROM Event e WHERE e.toDate >= current_date AND e.title LIKE %?1% ORDER BY e.fromDate")
    List<Event> findEventByToDateAndTitle(String query);

    @Query(value = "SELECT e FROM Event e WHERE e.fromDate > current_date")
    List<Event> findEventByFromDateAfter(String period);

    @Query(value = "SELECT e FROM Event e WHERE e.fromDate > current_date AND e.title LIKE %?1% ORDER BY e.fromDate")
    List<Event> findEventByFromDateAfterAndTitle(String query);
}

