package pl.sda.event.webapp.events;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

//@Controller
public class OnlyOneEvent {
    private EventRepository eventRepository;

   // @Autowired
    public OnlyOneEvent(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    //1. Wyświetlenie formularza
    //    -= wymyślenie URL -GET
    //@GetMapping("/event/add")
    public String showEventForm(Model model) {
        //    - stowrzyć HTML
        //    - może się okazać że do HTML potrzebujemy model
        model.addAttribute("th:object", new EventForm());
        return "nazwa formularza HTML";
    }

    //    -
    //2. Proces przyjmowania formularza czyli obsługa formularza
   // @PostMapping("/event/add") // to jest to samo co w action i method formularza w html<form
    public String handleAddEventFOrm(@ModelAttribute @Valid EventForm eventForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
       //2aa. zabezpieczyć urla

        //2a. Weryfikacja danych formularza
        // -określamy w obikecie DTO ograniczenia wymagania adnotacje w obiekcie DTO/FORM
        //- dodajemy BindingResult, zaraz za polem formularzem (WAŻNE jest to worek na błędzie
        // wymuszenie validacji formularza poprzez dodanie adnotacji @Valid
        // zwrócone w wniku błeędy validacji do użytkownika jeśli są błędy
        // sprawdzenie w knotrolerze i zwrócenie tego widoku co dla GET
        if (bindingResult.hasErrors()) {
            return "zwrócenie to samo co w GET MAPING";

        }
        // obsługa błędów html th:errors
        //2b. Konwersja na format skłądowania danych teczka wydarzenia
        Event event = new Event();
        event.setTitle(eventForm.getTitle());
        event.setFromDate(eventForm.getFromDate());
        event.setToDate(eventForm.getToDate());
        event.setDescription(eventForm.getDescription());
        //2c. Składowanie teczki- interface REPO
        eventRepository.save(event);
        redirectAttributes.addFlashAttribute("message", "Wydarzenie zostało dodane");
        return "redirect:/";
    }


    //2d. Wyświetlanie potwierdzzenie obsługi

}
