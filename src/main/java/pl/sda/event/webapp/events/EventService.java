package pl.sda.event.webapp.events;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.event.webapp.users.User;
import pl.sda.event.webapp.users.UserContextHolder;
import pl.sda.event.webapp.users.UserRegistrationRepository;

import java.util.List;

@Service
public class EventService {

    private EventRepository eventRepository;
    private UserRegistrationRepository userRegistrationRepository;
    private UserContextHolder userContextHolder;

    @Autowired
    public EventService(EventRepository eventRepository, UserRegistrationRepository userRegistrationRepository, UserContextHolder userContextHolder) {
        this.eventRepository = eventRepository;
        this.userRegistrationRepository = userRegistrationRepository;
        this.userContextHolder = userContextHolder;
    }

    public List<Event> showEvents(String query) {
        return eventRepository.findEventByEventAndTitleLike(query);
    }

    public List<Event> currentAndFutureEvents(String period, String query) {
        if (StringUtils.isBlank(query)){
            return eventRepository.findEventByToDate(period);
        }
        return eventRepository.findEventByToDateAndTitle(query);
    }

    public List<Event> futureEvents(String period, String query) {
        if (StringUtils.isBlank(query)) {
            return eventRepository.findEventByFromDateAfter(period);
        }
        return eventRepository.findEventByFromDateAfterAndTitle(query);
    }

    public void addEvent(EventForm eventForm) {
        Event event = eventFormToEvent(eventForm);
        User user = userRegistrationRepository.findUserByEmail(userContextHolder.getUserLoggedIn());
        event.setUser(user);
        eventRepository.save(event);
    }

    private Event eventFormToEvent(EventForm eventForm) {

        Event event = new Event();
        event.setTitle(eventForm.getTitle());
        event.setFromDate(eventForm.getFromDate());
        event.setToDate(eventForm.getToDate());
        event.setDescription(eventForm.getDescription());

        return event;
    }
}
