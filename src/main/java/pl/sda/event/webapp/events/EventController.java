package pl.sda.event.webapp.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.event.webapp.comments.Comment;
import pl.sda.event.webapp.comments.CommentForm;
import pl.sda.event.webapp.comments.CommentRepository;
import pl.sda.event.webapp.eventRegistration.EventRegistration;
import pl.sda.event.webapp.eventRegistration.EventRegistrationRepository;
import pl.sda.event.webapp.users.User;
import pl.sda.event.webapp.users.UserContextHolder;
import pl.sda.event.webapp.users.UserRegistrationRepository;

import javax.validation.Valid;
import java.util.Date;
import java.util.Optional;

@Controller
public class EventController {
    private EventService eventService;
    private EventRepository eventRepository;
    private CommentRepository commentRepository;
    private EventRegistrationRepository eventRegistrationRepository;
    private UserContextHolder userContextHolder;
    private UserRegistrationRepository userRegistrationRepository;

    @Autowired
    public EventController(EventService eventService, EventRepository eventRepository, CommentRepository commentRepository, EventRegistrationRepository eventRegistrationRepository, UserContextHolder userContextHolder, UserRegistrationRepository userRegistrationRepository) {
        this.eventService = eventService;
        this.eventRepository = eventRepository;
        this.commentRepository = commentRepository;
        this.eventRegistrationRepository = eventRegistrationRepository;


        this.userContextHolder = userContextHolder;
        this.userRegistrationRepository = userRegistrationRepository;
    }

    @GetMapping(value = "/addEvent")
    public String addEvent(Model model) {
        model.addAttribute("eventForm", new EventForm());
        return "addEvent";
    }

    @PostMapping(value = "/addEvent")
    public String addEvent(@ModelAttribute @Valid @DateTimeFormat EventForm eventForm, BindingResult bindingResult, Model model, String query) {
        if (bindingResult.hasErrors()) {
            return "addEvent";
        }
        eventService.addEvent(eventForm);
        return "events";
    }

    @GetMapping(value = "/events")
    public String showEvents(Model model, @RequestParam(required = false) String query, @RequestParam(required = false) String period) {

        if ("eventsList".equals(period)) {
            model.addAttribute("eventsList", eventService.showEvents(query));
        } else if ("currentFuture".equals(period)) {
            model.addAttribute("eventsList", eventService.currentAndFutureEvents(period, query));
        } else if ("future".equals(period)) {
            model.addAttribute("eventsList", eventService.futureEvents(period, query));
        }
        model.addAttribute("query", query);
        model.addAttribute("period", period);

        return "events";
    }


    @GetMapping(value = "/detailOfEvent/{id}")
    public String detailOfEvent(@PathVariable String id, Model model, RedirectAttributes redirectAttributes) {
        User user = userRegistrationRepository.findUserByEmail(userContextHolder.getUserLoggedIn());
        if(user!=null){
            EventRegistration emailAndEventId = eventRegistrationRepository.findFirstByUserEmailAndEventId(user.getEmail(), Integer.valueOf(id));
            model.addAttribute("userOnEvent", emailAndEventId);
        }
        model.addAttribute("oneEvent", eventRepository.findById(Integer.valueOf(id)).get());
        model.addAttribute("commentForm", new CommentForm());
        model.addAttribute("commentsOfEvent", commentRepository.findCommentByEventIdOrderByAddingDateDesc(Integer.valueOf(id)));
        model.addAttribute("allUserOnEvents", eventRegistrationRepository.findAll());

        return "detailOfEvent";
    }

    @PostMapping(value = "/detailOfEvent/{id}/comment")
    public String addComment(@PathVariable String id, @RequestParam String comment) {
        Comment commentEntity = new Comment();
        commentEntity.setComment(comment);
        commentEntity.setAddingDate(new Date());
        User user = userRegistrationRepository.findUserByEmail(userContextHolder.getUserLoggedIn());

        commentEntity.setUser(user);
        Optional<Event> event = eventRepository.findById(Integer.valueOf(id));
        commentEntity.setEvent(event.get());
        commentRepository.save(commentEntity);
        return "redirect:/detailOfEvent/" + id;
    }

    @PostMapping(value = "/detailOfEvent/{id}/eventRegistration")
    public String eventRegistration(@PathVariable String id, RedirectAttributes redirectAttributes) {
        EventRegistration eventRegistration = new EventRegistration();
        eventRegistration.setDateOfRegistration(new Date());

        User user = userRegistrationRepository.findUserByEmail(userContextHolder.getUserLoggedIn());
        eventRegistration.setUser(user);

        Optional<Event> event = eventRepository.findById(Integer.valueOf(id));
        eventRegistration.setEvent(event.get());

        if (eventRegistrationRepository.findFirstByUserEmail(user.getEmail())!=null){
            redirectAttributes.addFlashAttribute("msg", user.getEmail() + " jesteś już zapisany " + event.get().getTitle());
            return "redirect:/detailOfEvent/" + id;
        }
        eventRegistrationRepository.save(eventRegistration);
        redirectAttributes.addFlashAttribute("msg", user.getEmail() + " zapisałeś się na " + event.get().getTitle());

        return "redirect:/detailOfEvent/" + id;
    }

    @PostMapping(value = "/detailOfEvent/{id}/leaveEvent")
    String leaveEvent(@PathVariable String id, RedirectAttributes redirectAttributes, Model model) {

        User user = userRegistrationRepository.findUserByEmail(userContextHolder.getUserLoggedIn());
        EventRegistration emailAndEventId = eventRegistrationRepository.findFirstByUserEmailAndEventId(user.getEmail(), Integer.valueOf(id));
        eventRegistrationRepository.delete(emailAndEventId);
        model.addAttribute("userOnEvent", emailAndEventId);
        redirectAttributes.addFlashAttribute("msg", user.getEmail() + " opuściłeś wydarzenie");

        return "redirect:/detailOfEvent/" + id;
    }
}
