package pl.sda.event.webapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.sda.event.webapp.api.ApiEventDTO;
import pl.sda.event.webapp.api.ApiEventListDTO;
import pl.sda.event.webapp.events.Event;
import pl.sda.event.webapp.events.EventRepository;

import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(value = "/api")
public class EventRestController {

    private EventRepository eventRepository;

    @Autowired
    public EventRestController(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @GetMapping(value = "/events")
    public ResponseEntity<List<ApiEventListDTO>> getEvents() {
/*
        List<ApiEventListDTO> events = eventRepository.findAll()
                .stream()
                .filter(e -> e.getFromDate().after(new Date()))
                .map(e -> {
                    ApiEventListDTO apiEventListDTO = new ApiEventListDTO();
                    apiEventListDTO.setId(e.getId());
                    apiEventListDTO.setName(e.getTitle());
                    apiEventListDTO.setFromDate(e.getFromDate());
                    apiEventListDTO.setEndDate(e.getToDate());
                    return apiEventListDTO;
                })
                .collect(toList());

                */

        List<ApiEventListDTO> apiEventListDTOS = eventRepository.findEventByFromDateAfter(new Date().toString()).stream().map(event -> eventToApiList(event)).collect(toList());

        return ResponseEntity.ok(apiEventListDTOS);
    }

    @GetMapping(value = "/event/{id}")
    public ResponseEntity<ApiEventDTO> getApiEventById(@PathVariable Integer id) {
        ApiEventDTO apiEventDTO = eventRepository.findById(id).map(event -> eventToApiDTO(event)).get();
        return ResponseEntity.ok(apiEventDTO);
    }

    private ApiEventListDTO eventToApiList(Event event) {

        ApiEventListDTO apiEventListDTO = new ApiEventListDTO();
        apiEventListDTO.setId(event.getId());
        apiEventListDTO.setName(event.getTitle());
        apiEventListDTO.setFromDate(event.getFromDate());
        apiEventListDTO.setEndDate(event.getToDate());
        return apiEventListDTO;
    }

    private ApiEventDTO eventToApiDTO(Event event) {
        ApiEventDTO apiEventDTO = new ApiEventDTO();
        apiEventDTO.setId(event.getId());
        apiEventDTO.setName(event.getTitle());
        apiEventDTO.setStartDate(event.getFromDate());
        apiEventDTO.setEndDate(event.getToDate());
        apiEventDTO.setDescription(event.getDescription());
        return apiEventDTO;
    }

}
